json.array!(@trending_courses) do |trending_course|
  json.extract! trending_course, :id, :name, :description, :picture
  json.url trending_course_url(trending_course, format: :json)
end
