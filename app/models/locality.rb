class Locality < ActiveRecord::Base
  belongs_to :city
  has_many :contacts

  def self.save_localities
    CSV.foreach("public/cities_list.csv", :encoding => 'windows-1251:utf-8') do |row|
      Locality.find_or_create_by(locality: row[0], city_id: row[2])
    end
  end

end
