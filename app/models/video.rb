class Video < ActiveRecord::Base
  belongs_to :institute_profile
  attr_accessor :id_institute_profile

  validates_presence_of :url, :title, :description

  def get_video_id
    url = self.url
    url[/(?<=[?&]v=)[^&$]+/]
  end

  def get_thumbnail_url
    video_id = self.get_video_id
    return "http://img.youtube.com/vi/#{video_id}/0.jpg"
  end
end
