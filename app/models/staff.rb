class Staff < ActiveRecord::Base
  belongs_to :institute_profile
  attr_accessor :id_institute_profile
  validates_presence_of :name, :designation, :specialization, :experience
  validates_inclusion_of :experience, :in => 1..99
end
