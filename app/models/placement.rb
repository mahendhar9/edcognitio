class Placement < ActiveRecord::Base
  belongs_to :course
  belongs_to :institute_profile
  attr_accessor :id_institute_profile

  validates_presence_of :course_id, :student_name, :company_name, :year
end
