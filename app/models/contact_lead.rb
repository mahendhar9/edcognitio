class ContactLead < ActiveRecord::Base
  after_create :send_email
  belongs_to :institute_profile
  attr_accessor :id_institute_profile
  validates_presence_of :name, :email, :subject, :message

  def send_email
    BookingMailer.contact_lead(self).deliver_later
  end
end
