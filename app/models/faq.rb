class Faq < ActiveRecord::Base
  include Bootsy::Container
  belongs_to :institute_profile
  attr_accessor :id_institute_profile

  validates_presence_of :question, :answer
end
