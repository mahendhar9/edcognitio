class Course < ActiveRecord::Base
  validates_presence_of :name, :description, :course_sub_category_id, :full_description, :course_fee
  ratyrate_rateable "rate_course"
  mount_uploader :attachment, CourseAttachmentUploader
  belongs_to :institute_profile
  belongs_to :course_sub_category
  has_many :placements, dependent: :destroy
  attr_accessor :id_institute_profile, :course_category
end
