class Certificate < ActiveRecord::Base
  mount_uploader :picture, PictureUploader
  belongs_to :institute_profile
  attr_accessor :id_institute_profile

  validates_presence_of :picture, :title, :description
end
