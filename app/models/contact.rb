class Contact < ActiveRecord::Base
  geocoded_by :address
  after_validation :geocode, if: :address_changed?
  belongs_to :institute_profile
  belongs_to :locality
  attr_accessor :id_institute_profile

  validates_presence_of :address, :phone_number, :state, :locality_id
end
