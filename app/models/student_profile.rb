class StudentProfile < ActiveRecord::Base
  mount_uploader :profile_picture, ProfilePictureUploader

  belongs_to :user
end
