class Testimonial < ActiveRecord::Base
  mount_uploader :picture, TestimonialPictureUploader
  belongs_to :institute_profile
  attr_accessor :id_institute_profile

  validates_presence_of :name, :description
end
