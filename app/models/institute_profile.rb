class InstituteProfile < ActiveRecord::Base
  mount_uploader :cover_photo, CoverPhotoUploader
  mount_uploader :profile_picture, ProfilePictureUploader
  mount_uploader :brochure, CourseAttachmentUploader

  belongs_to :user

  has_many :staffs, dependent: :destroy
  has_many :courses, dependent: :destroy
  has_many :testimonials, dependent: :destroy
  has_many :pictures, dependent: :destroy
  has_many :certificates, dependent: :destroy
  has_many :placements, dependent: :destroy
  has_many :videos, dependent: :destroy
  has_many :contacts, dependent: :destroy
  has_many :faqs, dependent: :destroy
  has_many :contact_leads, dependent: :destroy

  attr_accessor :edit_institute_profile_id

  accepts_nested_attributes_for :staffs, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :courses, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :testimonials, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :pictures, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :certificates, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :placements, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :videos, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :contacts, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :faqs, reject_if: :all_blank, allow_destroy: true
end


