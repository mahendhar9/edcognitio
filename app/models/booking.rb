class Booking < ActiveRecord::Base
  after_create :send_email
  belongs_to :institute, class_name: "User"
  belongs_to :student, class_name: "User"

  def send_email
    BookingMailer.booking_details(self).deliver_later
  end
end
