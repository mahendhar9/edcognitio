class Picture < ActiveRecord::Base
  mount_uploader :picture, PictureUploader
  belongs_to :institute_profile

  validates_presence_of :picture, :description
end
