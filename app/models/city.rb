class City < ActiveRecord::Base
  has_many :localities, dependent: :destroy
end
