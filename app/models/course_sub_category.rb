class CourseSubCategory < ActiveRecord::Base
  has_many :courses, dependent: :destroy
  belongs_to :course_category
end
