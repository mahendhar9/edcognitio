class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  ratyrate_rater
  devise :database_authenticatable, :registerable, :omniauthable, :confirmable,
  :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :role, :phone_number, on: :create, :unless => Proc.new { |user| user.from_omniauth?}
  validates_presence_of :phone_number, on: :update 
  validates_confirmation_of :password
  validates_presence_of :role, :phone_number, on: :update, :if => :omniauth_profile
  
  has_one :institute_profile, dependent: :destroy
  has_one :student_profile, dependent: :destroy

  has_many :student_bookings, foreign_key: "student_id", class_name: "Booking", dependent: :destroy
  has_many :institute_bookings, foreign_key: "institute_id", class_name: "Booking", dependent: :destroy
  has_many :posts, dependent: :destroy
  
  #Used for validating role and phone number for omniauth users
  # attr_accessor :omniauth_role, :omniauth_phone

  module Role
    ADMIN = "Admin"
    INSTITUTE = "Institute"
    STUDENT = "Student"
  end

  def admin?
    self.role == User::Role::ADMIN
  end

  def institute?
    self.role == User::Role::INSTITUTE
  end

  def student?
    self.role == User::Role::STUDENT
  end

  def institute_profile?
    self.institute_profile.name != nil unless self.institute_profile.nil?
  end

  def student_profile?
    !self.student_profile.nil?    
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.provider = auth.provider
      user.uid = auth.uid
      user.skip_confirmation!
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        # user.attributes = params
        user.email = data["email"] if user.email.blank?
        user.valid?
      end
    end
  end

  def password_required?
    super && provider.blank?
  end

  def from_omniauth?
    provider && uid
  end

  def omniauth_profile
    provider && uid 
  end
end
