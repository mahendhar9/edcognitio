class CourseCategory < ActiveRecord::Base
  has_many :subcategories, class_name: "CourseSubCategory", dependent: :destroy
end
