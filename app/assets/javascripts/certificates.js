$(document).ready(function(){
  $('.certificate-pictures img').click(function() {
    $('#certificate-title').html($(this).data('title'));
    $('#certificate-description').html($(this).data('description'));
    $('#certificate-image img').attr('src', $(this).attr('src'));
  });
});