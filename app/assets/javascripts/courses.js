$(document).ready(function(){
  function readMore() {
    $(this).closest('.course-box').toggleClass('course-full-height');
    $(this).parent().find('.course-full-desc').toggleClass('toggle-full-desc');
    $(this).html($(this).text() == 'Read More...' ? 'Close...' : 'Read More...');
  }
  // $('body').on('click', '.course-name', readMore);
  $('body').on('click', '.read-more', readMore);

});