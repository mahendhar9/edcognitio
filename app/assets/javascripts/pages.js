$(document).on('turbolinks:load', function() {
  // $('#course-inst-autocomplete').hide();
  $('#home-autocomplete, #course-inst-autocomplete').bind('railsAutocomplete.select', function(event, data){
    $('#home-search-autocomplete').click(function() {
      console.log("Clicked");
      $('#home-search-form')[0].submit();
    });
  });


  $('#common-autocomplete, #course-institute-autocomplete input').bind('railsAutocomplete.select', function(event, data){
    $('#common-search-autocomplete').click(function() {
      $('#common-search-form')[0].submit();
    });
  });

  $('#mob-home-autocomplete, #mob-course-inst-autocomplete').bind('railsAutocomplete.select', function(event, data){
    $('#mob-home-search-autocomplete').click(function() {
      console.log("Clicked");
      $('#mobile-home-search-form')[0].submit();
    });
  });

  $('#menu-1').lazeemenu();
  var container = $(".sidebar.left");
  container.sidebar();

  $('#open-menu').click(function() {
    if(container.hasClass('open')) {
      container.removeClass('open');
      container.trigger("sidebar:close");
      $('#open-menu i').removeClass('fa-times-circle-o').addClass('fa-bars');
    } else {
      container.css('display', 'block');
      container.addClass('open');
      container.trigger("sidebar:open");
      $('#open-menu i').removeClass('fa-bars').addClass('fa-times-circle-o');
    }
  });

  function submitAutocomplete(selector, boolean) {
    $(selector).keydown(function(event){
      if(event.keyCode == 13 && !jQuery(this).data( "ui-autocomplete" ).menu.active) {
        if($(selector).val().length==0) {
          event.preventDefault();
          return false;
        } else {
          if(boolean == true) {
            $('#home-search-form')[0].submit();
          } else if(boolean == false) {
            $('#common-search-form')[0].submit();
          }
        }
      }
    });
  }

  submitAutocomplete("#home-autocomplete", true);
  submitAutocomplete("#course-inst-autocomplete", true);
  submitAutocomplete("#course-institute-autocomplete input", false);
  submitAutocomplete("#common-autocomplete", false);

  $('#autocomplete_select_course_institute').change(function(){
    if($('#autocomplete_select_course_institute').val() == "1") {
      console.log("Course");
      $('#home-autocomplete').show();
      $('#course-inst-autocomplete').hide();
      $('#course-institute-autocomplete').css('display','none');
      $('#common-autocomplete').show();
      $('#course-inst-autocomplete').val('');

    } else if($('#autocomplete_select_course_institute').val() == "2") {
      console.log("Institute");
      $('#home-autocomplete').hide();
      $('#course-inst-autocomplete').show();
      $('#course-institute-autocomplete').css('display','inline-block');
      $('#common-autocomplete').hide();
      $('#home-autocomplete').val('');
    }
  });

  $('#mob-select-autocomplete').change(function(){
    if($('#mob-select-autocomplete').val() == "1") {
      console.log("Course");
      $('#mob-home-autocomplete').show();
      $('#mob-course-inst-autocomplete').hide();
      $('#mob-course-inst-autocomplete').val('');

    } else if($('#mob-select-autocomplete').val() == "2") {
      console.log("Institute");
      $('#mob-home-autocomplete').hide();
      $('#mob-course-inst-autocomplete').show();
      $('#mob-home-autocomplete').val('');
    }
  });

  setTimeout(function() {
    $('#notice_wrapper').fadeOut("slow", function() {
      $(this).remove();
    })
  }, 1500);

    // show active tab on reload
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

    // remember the hash in the URL without jumping
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
     if(history.pushState) {
      history.pushState(null, null, '#'+$(e.target).attr('href').substr(1));
    } else {
      location.hash = '#'+$(e.target).attr('href').substr(1);
    }
  });


    // $('.closeall').click(function(){
    //   $('.panel-collapse.in')
    //   .collapse('hide');
    // });
    // $('.openall').click(function(){
    //   $('.panel-collapse:not(".in")')
    //   .collapse('show');
    // });

    // $("#menu-hover .dropdown").hover(            
    //   function() {
    //     $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
    //     $(this).toggleClass('open');        
    //   },
    //   function() {
    //     $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
    //     $(this).toggleClass('open');       
    //   });



$('.posts a[href*="#"]:not([href="#"])').click(function() {
  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  }
});

});



$(document).on('page:fetch',   function() { NProgress.start(); });
$(document).on('page:change',  function() { NProgress.done(); });
$(document).on('page:restore', function() { NProgress.remove(); });


