$(document).ready(function(){
  //Append youtube video after focusing out from the Youtube URL field.
  $('#videos').on('cocoon:after-insert', function(e, insertedItem) {
    selector = $(insertedItem).find('input[type="url"]').attr('id');
    console.log(selector);
    $("#" + selector).blur(function() {
      id = $("#" + selector).val().split('v=')[1];
      $(this).parent().find('iframe').remove();
      $(this).parent().parent().find('.append-video').html('<iframe src=' + "https://www.youtube.com/embed/" + id + "?rel=0&controls=0&showinfo=0&iv_load_policy=3" + ' frameborder="0" ></iframe>')
    });
  });

  $('#add-new-video a').click();


  function append_video(thisObj, video_id) {
    thisObj.parent().parent().find('.append-video').html('<iframe src=' + "https://www.youtube.com/embed/" + video_id + "?rel=0&controls=0&showinfo=0&iv_load_policy=3" + ' frameborder="0"  class="append-video"></iframe>');
  }

  // Prefetch videos on videos edit page
  if (window.location.href.indexOf("edit-videos") != -1) {
    $('input[type=url]').map(function(index) {
      id = this.id;
      if ($("#" + id).val()){
        video_id = $("#" + id).val().split('v=')[1];
        append_video($(this), video_id);
      }
      $("#" + id).blur(function(){
        video_id = $(this).val().split('v=')[1];
        $(this).parent().find('iframe').remove();
        append_video($(this), video_id);
      });

    });
  }

  //When clicked on video thumbnail, embed the youtube video.
  $('.other-videos img').click(function(){
    id = $(this).data('id');
    description = $(this).data('description');
    $('#' + id).on('shown.bs.modal', function (e) {
      iframe_src = "https://www.youtube.com/embed/" + id + "?autoplay=1&rel=0&controls=0&showinfo=0&iv_load_policy=3"
      $('#videos-tab ' + '#' + id + ' .modal-body iframe').attr('src', iframe_src)
    //   $('#video-tab .modal-body').html('<div class="youtube-player"><iframe src=' + "https://www.youtube.com/embed/" + id + "?autoplay=1&rel=0&controls=0&showinfo=0&iv_load_policy=3" + ' frameborder="0"  class="" id="youtube-iframe"></iframe></div><p class="p-modal">' + description + '</p>');
  });
    $('#' + id).on('hidden.bs.modal', function () {
      $('#videos-tab ' + '#' + id + ' .modal-body iframe').attr('src','')
    });
  });
}); 