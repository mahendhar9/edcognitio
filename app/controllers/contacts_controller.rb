class ContactsController < ApplicationController
  before_action :find_institute, only: [:new, :edit_contacts]

  def new
    @locations = Locality.all
    @cities = City.all
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    contact_params = params[:institute_profile][:contacts_attributes].values
    contact_params.each do |contact_param|
      @contact = @institute_profile.contacts.new
      @contact.state = contact_param[:state]
      @contact.pincode = contact_param[:pincode]
      @contact.address = contact_param[:address]
      @contact.locality_id = contact_param[:locality_id]
      @contact.phone_number = contact_param[:phone_number]
      if @contact.save
        count += 1
      end
    end
    if count < contact_params.count
      @cities = City.all
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'contact-us-tab'), notice: "Successfully added Contacts."
    end
  end

  def edit_contacts
    @cities = City.all
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(contact_parameters)
      redirect_to institute_page_path(anchor: 'contact-us-tab'), notice: "Successfully updated."
    else
      @cities = City.all
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_contacts'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end

  def contact_parameters
    params.require(:institute_profile).permit(:id, contacts_attributes: [:id, :city, :state, :pincode, :address, :phone_number, :latitude, :longitude, :locality_id, :_destroy])
  end
end
