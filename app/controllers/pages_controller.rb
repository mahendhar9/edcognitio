class PagesController < ApplicationController
  autocomplete :course_sub_category, :name
  autocomplete :institute_profile, :name

  include SmartListing::Helper::ControllerExtensions
  helper  SmartListing::Helper
  def home
    @trending_courses = TrendingCourse.all
  end

  #The form for the omniauth user to update role and phone number
  def complete_profile
    @user = User.find(current_user.id)
  end

  #When the above form is submitted, the below action is called
  def omniauth_next_step
    @user = current_user
    if params[:user][:role].present? && params[:user][:phone_number].present?
      @user.role = params[:user][:role]
      @user.phone_number = params[:user][:phone_number]
      @user.save
      if @user.institute?
        redirect_to new_institute_profile_path, notice: "Please fill your profile."
      else
        redirect_to new_student_profile_path, notice: "Please complete your profile."
      end
    else
      flash.notice =  "Please fill the fields."
      render 'complete_profile' 
    end
  end

  def institute_page
    if params[:city] && params[:locality] && params[:institute_name]
      id = params[:institute_name].split('-').last.to_i
      @institute_profile = InstituteProfile.find(id)
    else
      @institute_profile = current_user.institute_profile
    end
    @staffs = @institute_profile.staffs
    @testimonials = @institute_profile.testimonials
    @pictures = @institute_profile.pictures
    @certificates = @institute_profile.certificates
    @faqs = @institute_profile.faqs
    @first_certificate = @certificates.first if @certificates.any?
    #For 'Courses'
    @course_categories = CourseCategory.all
    @all_sub_categories = CourseSubCategory.all

    @all_courses = @institute_profile.courses
    @inst_sub_categorys_ids = @all_courses.collect{|i| i.course_sub_category_id}.uniq
    @inst_sub_categories = CourseSubCategory.where(id: @inst_sub_categorys_ids)

    #For 'Placements'
    # @uniq_courses = @all_courses.uniq{ |c| c.course_sub_category_id }
    @all_placement_years = @institute_profile.placements.select(:year).distinct
    #For 'Videos'
    @videos = @institute_profile.videos
    @first_video = @videos.first if @videos.any?
    #For 'Contacts'
    @contacts = @institute_profile.contacts
    @first_contact = @contacts.first if @contacts.any?
    @contact_lead = ContactLead.new

    #Real-time search for Courses
    course_scope = @institute_profile.courses.order(:created_at)
    course_scope = course_scope.where('name ILIKE ?', "%#{params[:search][:filter]}%") if params[:search] != nil && params[:search][:filter] != ""
    course_scope = course_scope.where(course_sub_category_id: params[:sub_category].to_i) if params[:sub_category].to_i != 0
    @courses = smart_listing_create :courses, course_scope, partial: "pages/all_courses"

    #Real-time search for placements
    placement_scope = @institute_profile.placements
    placement_scope = placement_scope.where(course_id: params[:course_name].to_i) if params[:course_name].to_i != 0
    placement_scope = placement_scope.where(year: params[:placement_year].to_i) if params[:placement_year].to_i != 0
    @placements = smart_listing_create :placements, placement_scope, partial: "pages/all_placements"
  end

  def search
    if params[:autocomplete] && params[:autocomplete][:course_sub_category_id] != ""
      search_scope = InstituteProfile.all.joins(:courses).where('courses.course_sub_category_id' => params[:autocomplete][:course_sub_category_id]) 
    elsif params[:autocomplete] && params[:autocomplete][:course_sub_category_id] == "" && params[:autocomplete][:course_sub_category_name] !=""
      inst_ids = Course.joins(:course_sub_category).where("course_sub_categories.name ilike '%#{params[:autocomplete][:course_sub_category_name]}%' ").map{|id| id.institute_profile_id}
      search_scope = InstituteProfile.where(id: inst_ids)
    elsif params[:autocomplete] && params[:autocomplete][:course_inst_id] == "" && params[:autocomplete][:institute_profile_name] != ""
      search_scope = InstituteProfile.where("name ilike '%#{params[:autocomplete][:institute_profile_name]}%'")
    elsif params[:autocomplete] && params[:autocomplete][:course_inst_id] != ""
      search_scope = InstituteProfile.where(id: params[:autocomplete][:course_inst_id].to_i)
    elsif params[:search_courses] == "true"
      search_scope = InstituteProfile.all.joins(:courses).where('courses.course_sub_category_id' => params[:course_sub_category_id])
    else
     search_scope = InstituteProfile.all
   end
   
   @course_categories = CourseCategory.all
   search_scope = search_scope.joins(:courses).where('courses.course_sub_category_id' => [params[:search][:sub_category_id].to_i]) if params[:search] != nil && params[:search][:sub_category_id].to_i != 0
   # search_scope = search_scope.joins(:certificates).where('certificates.id' => params[:certificates]-[""]) if params[:certificates] != nil && params[:certificates] != [""]
   search_scope = search_scope.where(placement_assistance: true) if params[:placement_assistance].to_i != 0
   search_scope = search_scope.joins(:contacts).where('contacts.locality_id' => params[:location]-[""]) if params[:location] != nil && params[:location] != [""]
   # search_scope = search_scope.joins(:contacts).where((params[:location]-[""]).map{|add| "contacts.address ilike '%#{add}%'"}.join(' or ')) if params[:location] != nil && params[:location] != [""]
   search_scope = search_scope.uniq
   @institutes = smart_listing_create :search, search_scope, partial: "pages/search_all"
 end

 def book_session
  course_id = params[:course]
  institute_id = InstituteProfile.find(params[:institute]).user_id
  student_id = User.find(params[:student]).id
  @booking = Booking.create(institute_id: institute_id, course_id: course_id, student_id: student_id)
  render nothing: true
end

def contact
end

def terms
end

def tag
  tag = params[:tag]
  @tag = tag.split('training')[0].chomp("-")
  @title = @tag.tr('-', ' ').capitalize
end

def all_course_sub_categories
  @categories = CourseCategory.all
end

end
