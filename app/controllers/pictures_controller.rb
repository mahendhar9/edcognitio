class PicturesController < ApplicationController
  before_action :find_institute, only: [:new, :edit_pictures]

  def new
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    picture_params = params[:institute_profile][:pictures_attributes].values
    picture_params.each do |picture_param|
      @picture = @institute_profile.pictures.new
      @picture.description = picture_param[:description]
      @picture.picture = picture_param[:picture]
      if @picture.save
        count += 1
      end
    end
    if count < picture_params.count
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'pictures-tab'), notice: "Successfully added Pictures."
    end
  end

  def edit_pictures
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(picture_parameters)
      redirect_to institute_page_path(anchor: 'pictures-tab'), notice: "Successfully updated."
    else
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_pictures'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end

  def picture_parameters
    params.require(:institute_profile).permit(:id, pictures_attributes: [:id, :description, :picture, :_destroy])
  end
end
