class StaffsController < ApplicationController
  before_action :find_institute, only: [:new, :edit_staff]
  
  def new
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    staff_params = params[:institute_profile][:staffs_attributes].values
    staff_params.each do |staff_param|
      @staff = @institute_profile.staffs.new
      @staff.name = staff_param[:name]
      @staff.designation = staff_param[:designation]
      @staff.experience = staff_param[:experience]
      @staff.specialization = staff_param[:specialization]
      if @staff.save
        count += 1
      end
    end
    if count < staff_params.count
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'staffs-tab'), notice: "Successfully added Staffs."
    end
  end

  def edit_staff
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(staff_parameters)
      redirect_to institute_page_path(anchor: 'staffs-tab'), notice: "Successfully updated."
    else
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_staff'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end

  def staff_parameters
    params.require(:institute_profile).permit(:id, staffs_attributes: [:id, :name, :specialization, :experience, :designation, :_destroy])
  end
end
