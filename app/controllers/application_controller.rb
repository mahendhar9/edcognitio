class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :resource_name, :resource, :devise_mapping

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:role, :phone_number, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.permit(:sign_in) { |u| u.permit( :email, :password, :remember_me) }
    devise_parameter_sanitizer.permit(:account_update) {|u| u.permit(:phone_number, :email, :password, :password_confirmation)}
  end

  def after_sign_in_path_for(resource_or_scope)
    if current_user.phone_number.nil? && current_user.provider.present?
      complete_profile_path
    else
      sign_in_url = new_user_session_url
      sign_up_url = new_user_registration_url
      if request.referer == sign_in_url || sign_up_url
        super
      else
        stored_location_for(resource) || request.referer || root_path
      end
    end
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

end
