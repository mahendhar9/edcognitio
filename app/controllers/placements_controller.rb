class PlacementsController < ApplicationController
  before_action :find_institute, only: [:new, :edit_placements]

  def new
    @institute_courses = @institute_profile.courses
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    placement_params = params[:institute_profile][:placements_attributes].values
    placement_params.each do |placement_param|
      @placement = @institute_profile.placements.new
      @placement.student_name = placement_param[:student_name]
      @placement.company_name = placement_param[:company_name]
      @placement.year = placement_param[:year]
      @placement.course_id = placement_param[:course_id]
      if @placement.save
        count += 1
      end
    end
    if count < placement_params.count
      @institute_courses = @institute_profile.courses
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'placements-tab'), notice: "Successfully added placements."
    end
  end

  def edit_placements
    @institute_courses = @institute_profile.courses
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(placement_parameters)
      redirect_to institute_page_path(anchor: 'placements-tab'), notice: "Successfully updated."
    else
      @institute_courses = @institute_profile.courses
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_placements'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end
  
  def placement_parameters
    params.require(:institute_profile).permit(:id, placements_attributes: [:id, :student_name, :company_name, :year, :course_id, :_destroy])
  end
end
