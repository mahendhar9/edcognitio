class TestimonialsController < ApplicationController
  before_action :find_institute, only: [:new, :edit_testimonials]

  def new
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    testimonial_params = params[:institute_profile][:testimonials_attributes].values
    testimonial_params.each do |testimonial_param|
      @testimonial = @institute_profile.testimonials.new
      @testimonial.name = testimonial_param[:name]
      @testimonial.description = testimonial_param[:description]
      @testimonial.picture = testimonial_param[:picture]
      if @testimonial.save
        count += 1
      end
    end
    if count < testimonial_params.count
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'testimonials-tab'), notice: "Successfully added Testimonials."
    end
  end

  def edit_testimonials
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(testimonial_parameters)
      redirect_to institute_page_path(anchor: 'testimonials-tab'), notice: "Successfully updated."
    else
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_testimonials'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end
  
  def testimonial_parameters
    params.require(:institute_profile).permit(:id, testimonials_attributes: [:id, :name, :description, :picture, :_destroy])
  end

end
