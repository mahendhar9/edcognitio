class TrendingCoursesController < ApplicationController
  before_action :set_trending_course, only: [:show, :edit, :update, :destroy]

  # GET /trending_courses
  # GET /trending_courses.json
  def index
    @trending_courses = TrendingCourse.all
  end

  # GET /trending_courses/1
  # GET /trending_courses/1.json
  def show
  end

  # GET /trending_courses/new
  def new
    @trending_course = TrendingCourse.new
    @course_categories = CourseCategory.all
  end

  # GET /trending_courses/1/edit
  def edit
    @course_categories = CourseCategory.all
  end

  # POST /trending_courses
  # POST /trending_courses.json
  def create
    @trending_course = TrendingCourse.new(trending_course_params)

    respond_to do |format|
      if @trending_course.save
        format.html { redirect_to @trending_course, notice: 'Trending course was successfully created.' }
        format.json { render :show, status: :created, location: @trending_course }
      else
        format.html { render :new }
        format.json { render json: @trending_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trending_courses/1
  # PATCH/PUT /trending_courses/1.json
  def update
    respond_to do |format|
      if @trending_course.update(trending_course_params)
        format.html { redirect_to @trending_course, notice: 'Trending course was successfully updated.' }
        format.json { render :show, status: :ok, location: @trending_course }
      else
        format.html { render :edit }
        format.json { render json: @trending_course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trending_courses/1
  # DELETE /trending_courses/1.json
  def destroy
    @trending_course.destroy
    respond_to do |format|
      format.html { redirect_to trending_courses_url, notice: 'Trending course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trending_course
      @trending_course = TrendingCourse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trending_course_params
      params.require(:trending_course).permit(:name, :description, :picture, :course_sub_category_id)
    end
end
