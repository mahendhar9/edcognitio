class StudentProfilesController < ApplicationController
  before_action :find_profile, only: [:edit, :update, :show]

  def new
    @student_profile = StudentProfile.new
  end

  def create
    @student_profile = current_user.create_student_profile(student_profile_params)
    if @student_profile.save
      redirect_to @student_profile, notice: "Created Profile"
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    @student_profile.update_attributes(student_profile_params)
    redirect_to institute_page_path, notice: "Successfully updated Profile."
  end

  private

  def find_profile
    if (current_user && current_user.role == User::Role::STUDENT)
      @student_profile = current_user.student_profile 
    else
      redirect_to root_path
    end
  end

  def student_profile_params
    params.require(:student_profile).permit(:name, :profile_picture, :phone_number, :bio, :city, :state)
  end

end
