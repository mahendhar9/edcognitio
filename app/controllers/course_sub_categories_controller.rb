class CourseSubCategoriesController < ApplicationController
  before_action :set_course_sub_category, only: [:show, :edit, :update, :destroy]
  def new
    @categories = CourseCategory.all
    @subcategory = CourseSubCategory.new
  end

  def index
    @course_sub_categories = CourseSubCategory.all
  end

  def create
    @subcategory = CourseSubCategory.new(subcategory_params)
    if @subcategory.save
      redirect_to new_course_sub_category_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @course_sub_category.update(subcategory_params)
        format.html { redirect_to course_sub_categories_path, notice: 'Course category was successfully updated.' }
        format.json { render :show, status: :ok, location: @course_sub_category }
      else
        format.html { render :edit }
        format.json { render json: @course_sub_category.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @course_sub_category.destroy
    respond_to do |format|
      format.html { redirect_to course_sub_categories_path, notice: 'Course category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def subcategory_params
    params.require(:course_sub_category).permit(:name, :description, :course_category_id)
  end

  def set_course_sub_category
    @course_sub_category = CourseSubCategory.find(params[:id])
  end
end
