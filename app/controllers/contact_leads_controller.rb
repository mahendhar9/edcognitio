class ContactLeadsController < ApplicationController
  def new
    @contact_lead = ContactLead.new
  end

  def create
    @institute_profile = InstituteProfile.find(params[:contact_lead][:id_institute_profile])
    @contact_lead = @institute_profile.contact_leads.create(lead_params)
    respond_to do |format|
      if @contact_lead.save
        format.js
      else
        format.html {redirect_to root_path }
      end
    end 
  end

  def edit
  end

  private
  def lead_params
    params.require(:contact_lead).permit(:name, :email, :subject, :message, :id_institute_profile)
  end

end
