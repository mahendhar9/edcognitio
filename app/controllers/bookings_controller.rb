class BookingsController < ApplicationController
  def create
  # institute = User who has institute profile
  institute = InstituteProfile.find(params[:institute_id]).user_id
  @booking = Booking.create(institute_id: institute, course_id: params[:course_id], student_id: params[:student_id])
  respond_to do |format|
    if @booking.save
      format.js
    else
      format.html {redirect_to root_path}
    end
  end
end
end
