class InstituteProfilesController < ApplicationController
  before_action :auth_dashboard, only: [:new, :dashboard, :edit, :update, :contact_leads]

  def new
    @institute_profile = InstituteProfile.new
  end

  def create
    @institute_profile = current_user.create_institute_profile(institute_profile_params)
    if @institute_profile.save
      redirect_to dashboard_path(id: @institute_profile.id ), notice: "Created Profile"
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    @institute_profile.update_attributes(institute_profile_params)
    redirect_to institute_page_path, notice: "Successfully updated Profile."
  end

  def dashboard
  end

  def contact_leads
    @leads = ContactLead.where(institute_profile_id: @institute_profile).order('created_at DESC')
  end

  def analytics
    id = current_user.institute_profile.id
    a = Ahoy::Event.where(name: id.to_s).where(user_id: nil) | Ahoy::Event.where(name: id.to_s).where.not(user_id: current_user.id)
    b = a.map{|i| i.id}
    if params[:days] == "7"
      views = Ahoy::Event.where(id: b).where('time >= ?', Time.now - 7.days).where('time <= ?', Time.now)
      @visits = views.group_by_day(:time, time_zone: "Chennai",format: "%a, %e")
      @time = "7 days"
      @views = views.count
    elsif params[:days] == "30"
      views = Ahoy::Event.where(id: b).where('time >= ?', Time.now - 30.days).where('time <= ?', Time.now)
      @visits = views.group_by_day(:time, time_zone: "Chennai",format: "%a, %e")
      @time = "30 days"
      @views = views.count
    else
      views = Ahoy::Event.where(id: b).where('time >= ?', Time.now - 24.hours).where('time <= ?', Time.now)
      @visits = views.group_by_hour(:time, time_zone: "Chennai",format: "%H:%M")
      @time = "1 day"
      @views = views.count
    end

    contact_leads = ContactLead.where('created_at > ?', Time.now-30.days).where('created_at < ?', Time.now)
    @contact_leads = contact_leads.group_by_day(:created_at, time_zone: "Chennai",format: "%a, %e")
    @contact_lead_count = contact_leads.count
  end

  private

  def find_profile
    # @institute_profile = InstituteProfile.find(params[:id])
    @institute_profile = current_user.institute_profile

  end

  def institute_profile_params
    params.require(:institute_profile).permit(:name, :established, :cover_photo, :profile_picture, :placement_assistance, :tagline, :courses_available, :students_strength, :placement_rate, :institute_bio, :brochure, staffs_attributes: [:name, :specialization, :experience, :designation])
  end

  def auth_dashboard
    if (current_user && current_user.role == User::Role::INSTITUTE)
      @institute_profile = current_user.institute_profile
    else
      redirect_to root_path
    end
  end

  
end
