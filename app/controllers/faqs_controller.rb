class FaqsController < ApplicationController
  before_action :find_institute, only: [:new, :edit_faqs]

  def new
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    faq_params = params[:institute_profile][:faqs_attributes].values
    faq_params.each do |faq_param|
      @faq = @institute_profile.faqs.new
      @faq.question = faq_param[:question]
      @faq.answer = faq_param[:answer]
      if @faq.save
        count += 1
      end
    end
    if count < faq_params.count
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'faq-tab'), notice: "Successfully added FAQs."
    end
  end

  def edit_faqs
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(faq_parameters)
      redirect_to institute_page_path(anchor: 'faq-tab'), notice: "Successfully updated."
    else
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_faqs'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end

  def faq_parameters
    params.require(:institute_profile).permit(:id, faqs_attributes: [:id, :question, :answer, :_destroy])
  end

end
