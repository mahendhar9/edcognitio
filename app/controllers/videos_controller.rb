class VideosController < ApplicationController
  before_action :find_institute, only: [:new, :edit_videos]

  def new
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    video_params = params[:institute_profile][:videos_attributes].values
    video_params.each do |video_param|
      @video = @institute_profile.videos.new
      @video.title = video_param[:title]
      @video.description = video_param[:description]
      @video.url = video_param[:url]
      if @video.save
        count += 1
      end
    end
    if count < video_params.count
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'videos-tab'), notice: "Successfully added Videos."
    end
  end

  def edit_videos
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(video_parameters)
      redirect_to institute_page_path(anchor: 'videos-tab'), notice: "Successfully updated."
    else
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_videos'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end
  
  def video_parameters
    params.require(:institute_profile).permit(:id, videos_attributes: [:id, :title, :description, :url, :_destroy])
  end
end
