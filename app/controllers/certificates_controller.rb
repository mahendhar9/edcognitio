class CertificatesController < ApplicationController
  before_action :find_institute, only: [:new, :edit_certificates]

  def new
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    certificate_params = params[:institute_profile][:certificates_attributes].values
    certificate_params.each do |certificate_param|
      @certificate = @institute_profile.certificates.new
      @certificate.title = certificate_param[:title]
      @certificate.description = certificate_param[:description]
      @certificate.picture = certificate_param[:picture]
      if @certificate.save
        count += 1
      end
    end
    if count < certificate_params.count
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'certificates-tab'), notice: "Successfully added Certificates."
    end
  end

  def edit_certificates
  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(certificate_parameters)
      redirect_to institute_page_path(anchor: 'certificates-tab'), notice: "Successfully updated."
    else
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_certificates'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end

  def certificate_parameters
    params.require(:institute_profile).permit(:id, certificates_attributes: [:id, :title, :description, :picture, :_destroy])
  end
end
