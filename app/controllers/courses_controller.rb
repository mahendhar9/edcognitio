class CoursesController < ApplicationController
  before_action :find_institute, only: [:new, :edit_courses]

  def new
    @course_categories = CourseCategory.all
  end

  def create
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    count = 0
    course_params = params[:institute_profile][:courses_attributes].values
    course_params.each do |course_param|
      @course = @institute_profile.courses.new
      @course.name = course_param[:name]
      @course.course_sub_category_id = course_param[:course_sub_category_id]
      @course.offer_demo = course_param[:offer_demo]
      @course.course_fee = course_param[:course_fee]
      @course.description = course_param[:description]
      @course.attachment = course_param[:attachment]
      @course.full_description = course_param[:full_description]
      if @course.save
        count += 1
      end
    end
    if count < course_params.count
      @course_categories = CourseCategory.all
      flash.now[:notice] =  "Please fix the following errors (new)"
      render 'new'
    else
      redirect_to institute_page_path(anchor: 'courses-tab'), notice: "Successfully added Courses."
    end
  end

  def edit_courses
    @course_categories = CourseCategory.all
    @course_sub_categories = CourseSubCategory.all

  end

  def update
    @institute_profile = InstituteProfile.find(params[:institute_profile][:id_institute_profile]
      )
    if @institute_profile.update_attributes(course_parameters)
      redirect_to institute_page_path(anchor: 'courses-tab'), notice: "Successfully updated."
    else
      @course_categories = CourseCategory.all
      flash.now[:notice] =  "Please fix the following errors (edit)"
      render 'edit_courses'
    end
  end

  private
  def find_institute
    @institute_profile = current_user.institute_profile
  end

  def course_parameters
    params.require(:institute_profile).permit(:id, courses_attributes: [:id, :name, :description, :offer_demo, :course_fee, :full_description, :attachment, :course_sub_category_id, :_destroy])
  end
end
