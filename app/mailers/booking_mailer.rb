class BookingMailer < ApplicationMailer

  def booking_details(booking)
    @course = Course.find(booking.course_id).name
    @user_email = User.find(booking.student_id).email
    @institute_email = User.find(booking.institute_id).email
    @institute_name = InstituteProfile.find_by(user_id: booking.institute_id).name
    @url  = 'http://www.gmail.com'
    mail(to: @user_email, subject: 'You have booked a free session')
  end

  def contact_lead(lead)
    @institute = InstituteProfile.find(lead.institute_profile_id)
    @institute_email = User.find(@institute.user_id).email
    @lead = lead
    mail(to: @institute_email, subject: 'You have a new lead!')
  end
end
