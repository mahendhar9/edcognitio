require 'test_helper'

class TrendingCoursesControllerTest < ActionController::TestCase
  setup do
    @trending_course = trending_courses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:trending_courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create trending_course" do
    assert_difference('TrendingCourse.count') do
      post :create, trending_course: { description: @trending_course.description, name: @trending_course.name, picture: @trending_course.picture }
    end

    assert_redirected_to trending_course_path(assigns(:trending_course))
  end

  test "should show trending_course" do
    get :show, id: @trending_course
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @trending_course
    assert_response :success
  end

  test "should update trending_course" do
    patch :update, id: @trending_course, trending_course: { description: @trending_course.description, name: @trending_course.name, picture: @trending_course.picture }
    assert_redirected_to trending_course_path(assigns(:trending_course))
  end

  test "should destroy trending_course" do
    assert_difference('TrendingCourse.count', -1) do
      delete :destroy, id: @trending_course
    end

    assert_redirected_to trending_courses_path
  end
end
