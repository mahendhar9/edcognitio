Rails.application.routes.draw do

  resources :posts
  resources :trending_courses
  mount RailsAdmin::Engine => '/edcognitio_admin', as: 'rails_admin'
  resources :cities
  resources :course_categories
  resources :course_sub_categories
  resources :contact_leads, only: [:create, :destroy]
  post '/rate' => 'rater#create', :as => 'rate'
  mount Bootsy::Engine => '/bootsy', as: 'bootsy'
  mount Browserlog::Engine => '/logs'

  # RailsAdmin.config do |config|
  #   config.authorize_with do
  #     redirect_to main_app.root_path 
  #     # unless current_user.role == User::Role::ADMIN
  #   end
  # end

  get 'blog' => 'posts#index', as: 'blog'
  get 'blog/:id' => 'posts#show', as: 'blog_post'
  get 'contact' => 'pages#contact', as: 'contact_us'
  get 'terms-and-conditions' => 'pages#terms', as: 'terms'
  
  resources :localities
  root 'pages#home'

  resources :pages do
    get :autocomplete_course_sub_category_name, :on => :collection
    get :autocomplete_institute_profile_name, :on => :collection
  end

  get 'complete-profile' => 'pages#complete_profile', as: 'complete_profile'
  put 'complete-profile' => 'pages#omniauth_next_step', as: 'omniauth_next_step'

  devise_for :users, controllers: {registrations: "registrations", :omniauth_callbacks => "users/omniauth_callbacks", confirmations: "confirmations"}
  resources :institute_profiles, only: [:new, :create, :update]
  get 'institute-profile/edit' => 'institute_profiles#edit', as: 'edit_institute_profile'
  get 'student-profile/edit' => 'student_profiles#edit', as: 'edit_student_profile'

  resources :student_profiles

  resources :staffs, only: [:new, :create, :update]
  get 'edit-staff' => 'staffs#edit_staff', as: 'edit_staff'

  resources :courses, only: [:new, :create, :update]
  get 'edit-courses' => 'courses#edit_courses', as: 'edit_courses'

  resources :testimonials, only: [:new, :create, :update]
  get 'edit-testimonials' => 'testimonials#edit_testimonials', as: 'edit_testimonials'

  resources :pictures, only: [:new, :create, :update]
  get 'edit-pictures' => 'pictures#edit_pictures', as: 'edit_pictures'

  resources :certificates, only: [:new, :create, :update]
  get 'edit-certificates' => 'certificates#edit_certificates', as: 'edit_certificates'

  resources :placements, only: [:new, :create, :update]
  get 'edit-placements' => 'placements#edit_placements', as: 'edit_placements'

  resources :videos, only: [:new, :create, :update]
  get 'edit-videos' => 'videos#edit_videos', as: 'edit_videos'

  resources :contacts, only: [:new, :create, :update]
  get 'edit-contacts' => 'contacts#edit_contacts', as: 'edit_contacts' 

  resources :faqs, only: [:new, :create, :update]
  get 'edit-faqs' => 'faqs#edit_faqs', as: 'edit_faqs'

  get 'edit_course_subcategories' => 'course_sub_categories#edit_subcategories', as: 'edit_subcategories'

  resources :bookings, only: [:create]
  get 'dashboard' => 'institute_profiles#dashboard', as: 'dashboard'
  get 'institute-page' => 'pages#institute_page', as: 'institute_page'
  get '/:city/:locality/:institute_name' => 'pages#institute_page', as: 'individual_institute'
  get 'institutes/search' => 'pages#search', as: 'search'

  get 'all-courses' => 'pages#all_course_sub_categories', as: 'all_courses'
  get 'leads' => 'institute_profiles#contact_leads', as: 'lead' 
  get 'analytics' => 'institute_profiles#analytics', as: 'analytics' 

  get '/:tag' => 'pages#tag', as: 'tag'

  # get 'get-preview-images/:id' => 'pages#preview_edit_institute_images', as: 'preview_images'
end
