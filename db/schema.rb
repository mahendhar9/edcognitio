# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160623125823) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ahoy_events", force: :cascade do |t|
    t.integer  "visit_id"
    t.integer  "user_id"
    t.string   "name"
    t.json     "properties"
    t.datetime "time"
  end

  add_index "ahoy_events", ["name", "time"], name: "index_ahoy_events_on_name_and_time", using: :btree
  add_index "ahoy_events", ["user_id", "name"], name: "index_ahoy_events_on_user_id_and_name", using: :btree
  add_index "ahoy_events", ["visit_id", "name"], name: "index_ahoy_events_on_visit_id_and_name", using: :btree

  create_table "average_caches", force: :cascade do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "avg",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bookings", force: :cascade do |t|
    t.integer  "institute_id"
    t.integer  "student_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "course_id"
  end

  create_table "bootsy_image_galleries", force: :cascade do |t|
    t.integer  "bootsy_resource_id"
    t.string   "bootsy_resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bootsy_images", force: :cascade do |t|
    t.string   "image_file"
    t.integer  "image_gallery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "certificates", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "picture"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "certificates", ["institute_profile_id"], name: "index_certificates_on_institute_profile_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_leads", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "subject"
    t.text     "message"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "contact_leads", ["institute_profile_id"], name: "index_contact_leads_on_institute_profile_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.text     "address"
    t.string   "phone_number"
    t.string   "state"
    t.integer  "pincode"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "locality_id"
  end

  add_index "contacts", ["institute_profile_id"], name: "index_contacts_on_institute_profile_id", using: :btree

  create_table "course_categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "course_sub_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "course_category_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "course_sub_categories", ["course_category_id"], name: "index_course_sub_categories_on_course_category_id", using: :btree

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "institute_profile_id"
    t.integer  "course_sub_category_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "attachment"
    t.text     "full_description"
    t.string   "course_fee"
    t.boolean  "offer_demo"
  end

  add_index "courses", ["course_sub_category_id"], name: "index_courses_on_course_sub_category_id", using: :btree
  add_index "courses", ["institute_profile_id"], name: "index_courses_on_institute_profile_id", using: :btree

  create_table "faqs", force: :cascade do |t|
    t.string   "question"
    t.text     "answer"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "faqs", ["institute_profile_id"], name: "index_faqs_on_institute_profile_id", using: :btree

  create_table "institute_profiles", force: :cascade do |t|
    t.string   "name"
    t.date     "established"
    t.string   "cover_photo"
    t.string   "profile_picture"
    t.string   "tagline"
    t.integer  "courses_available"
    t.integer  "students_strength"
    t.integer  "placement_rate"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.boolean  "placement_assistance"
    t.string   "brochure"
    t.text     "institute_bio"
  end

  add_index "institute_profiles", ["user_id"], name: "index_institute_profiles_on_user_id", using: :btree

  create_table "localities", force: :cascade do |t|
    t.string   "locality"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "city_id"
  end

  create_table "overall_averages", force: :cascade do |t|
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "overall_avg",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pictures", force: :cascade do |t|
    t.text     "description"
    t.string   "picture"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "pictures", ["institute_profile_id"], name: "index_pictures_on_institute_profile_id", using: :btree

  create_table "placements", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "year"
    t.string   "student_name"
    t.string   "company_name"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "placements", ["course_id"], name: "index_placements_on_course_id", using: :btree
  add_index "placements", ["institute_profile_id"], name: "index_placements_on_institute_profile_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "rates", force: :cascade do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "stars",         null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rates", ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type", using: :btree
  add_index "rates", ["rater_id"], name: "index_rates_on_rater_id", using: :btree

  create_table "rating_caches", force: :cascade do |t|
    t.integer  "cacheable_id"
    t.string   "cacheable_type"
    t.float    "avg",            null: false
    t.integer  "qty",            null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rating_caches", ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type", using: :btree

  create_table "staffs", force: :cascade do |t|
    t.string   "name"
    t.string   "designation"
    t.string   "specialization"
    t.integer  "experience"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "staffs", ["institute_profile_id"], name: "index_staffs_on_institute_profile_id", using: :btree

  create_table "student_profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "profile_picture"
    t.string   "phone_number"
    t.text     "bio"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "student_profiles", ["user_id"], name: "index_student_profiles_on_user_id", using: :btree

  create_table "testimonials", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "picture"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "testimonials", ["institute_profile_id"], name: "index_testimonials_on_institute_profile_id", using: :btree

  create_table "trending_courses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "picture"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "course_sub_category_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "role"
    t.string   "phone_number"
    t.string   "provider"
    t.string   "uid"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "videos", force: :cascade do |t|
    t.string   "url"
    t.string   "title"
    t.text     "description"
    t.integer  "institute_profile_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "videos", ["institute_profile_id"], name: "index_videos_on_institute_profile_id", using: :btree

  create_table "visits", force: :cascade do |t|
    t.string   "visit_token"
    t.string   "visitor_token"
    t.string   "ip"
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "referring_domain"
    t.string   "search_keyword"
    t.string   "browser"
    t.string   "os"
    t.string   "device_type"
    t.integer  "screen_height"
    t.integer  "screen_width"
    t.string   "country"
    t.string   "region"
    t.string   "city"
    t.string   "postal_code"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "utm_campaign"
    t.datetime "started_at"
  end

  add_index "visits", ["user_id"], name: "index_visits_on_user_id", using: :btree
  add_index "visits", ["visit_token"], name: "index_visits_on_visit_token", unique: true, using: :btree

  add_foreign_key "certificates", "institute_profiles"
  add_foreign_key "contact_leads", "institute_profiles"
  add_foreign_key "contacts", "institute_profiles"
  add_foreign_key "course_sub_categories", "course_categories"
  add_foreign_key "courses", "course_sub_categories"
  add_foreign_key "courses", "institute_profiles"
  add_foreign_key "faqs", "institute_profiles"
  add_foreign_key "institute_profiles", "users"
  add_foreign_key "pictures", "institute_profiles"
  add_foreign_key "placements", "courses"
  add_foreign_key "placements", "institute_profiles"
  add_foreign_key "posts", "users"
  add_foreign_key "staffs", "institute_profiles"
  add_foreign_key "student_profiles", "users"
  add_foreign_key "testimonials", "institute_profiles"
  add_foreign_key "videos", "institute_profiles"
end
