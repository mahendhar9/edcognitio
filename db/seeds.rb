(16..40).each do |n|
  User.create(email: "institute#{n}@example.com", role: User::Role::INSTITUTE, password: "password", password_confirmation: "password", confirmed_at: Time.now, phone_number: "8341407914")
  puts "User #{n}"
end

# (1..5).each do |n|
#   User.find_or_create_by(email: "student#{n}@example.com", role: User::Role::STUDENT, password: "password", password_confirmation: "password", confirmed_at: Time.now, phone_number: "8341407914")
#   puts "User #{n}"
# end
