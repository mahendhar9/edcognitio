class AddCourseFeeToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :course_fee, :string
  end
end
