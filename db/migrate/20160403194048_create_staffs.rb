class CreateStaffs < ActiveRecord::Migration
  def change
    create_table :staffs do |t|
      t.string :name
      t.string :designation
      t.string :specialization
      t.integer :experience
      t.references :institute_profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
