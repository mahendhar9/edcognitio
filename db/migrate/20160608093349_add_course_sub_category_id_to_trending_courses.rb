class AddCourseSubCategoryIdToTrendingCourses < ActiveRecord::Migration
  def change
    add_column :trending_courses, :course_sub_category_id, :integer
  end
end
