class CreateInstituteProfiles < ActiveRecord::Migration
  def change
    create_table :institute_profiles do |t|
      t.string :name
      t.date :established
      t.string :cover_photo
      t.string :profile_picture
      t.string :tagline
      t.integer :courses_available
      t.integer :students_strength
      t.integer :placement_rate
      t.references :user, index: true, foreign_key: true


      t.timestamps null: false
    end
  end
end
