class CreateTrendingCourses < ActiveRecord::Migration
  def change
    create_table :trending_courses do |t|
      t.string :name
      t.string :description
      t.string :picture

      t.timestamps null: false
    end
  end
end
