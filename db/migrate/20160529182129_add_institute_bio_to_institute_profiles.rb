class AddInstituteBioToInstituteProfiles < ActiveRecord::Migration
  def change
    add_column :institute_profiles, :institute_bio, :text
  end
end
