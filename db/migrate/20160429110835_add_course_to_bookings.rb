class AddCourseToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :course_id, :integer
  end
end
