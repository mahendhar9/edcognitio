class CreateStudentProfiles < ActiveRecord::Migration
  def change
    create_table :student_profiles do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :profile_picture
      t.string :phone_number
      t.text :bio
      t.string :city
      t.string :state

      t.timestamps null: false
    end
  end
end
