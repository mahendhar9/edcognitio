class CreateContactLeads < ActiveRecord::Migration
  def change
    create_table :contact_leads do |t|
      t.string :name
      t.string :email
      t.string :subject
      t.text :message
      t.references :institute_profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
