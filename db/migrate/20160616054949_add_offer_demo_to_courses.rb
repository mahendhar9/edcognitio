class AddOfferDemoToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :offer_demo, :boolean
  end
end
