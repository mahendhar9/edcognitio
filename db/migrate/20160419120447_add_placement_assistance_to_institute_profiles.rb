class AddPlacementAssistanceToInstituteProfiles < ActiveRecord::Migration
  def change
    add_column :institute_profiles, :placement_assistance, :boolean
  end
end
