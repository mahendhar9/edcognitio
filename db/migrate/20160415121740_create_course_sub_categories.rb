class CreateCourseSubCategories < ActiveRecord::Migration
  def change
    create_table :course_sub_categories do |t|
      t.string :name
      t.string :description
      t.references :course_category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
