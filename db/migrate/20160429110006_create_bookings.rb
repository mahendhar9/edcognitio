class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :institute_id
      t.integer :student_id

      t.timestamps null: false
    end
  end
end
