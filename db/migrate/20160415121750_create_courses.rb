class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.string :description
      t.references :institute_profile, index: true, foreign_key: true
      t.references :course_sub_category, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
