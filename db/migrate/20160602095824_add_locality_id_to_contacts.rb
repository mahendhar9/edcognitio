class AddLocalityIdToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :locality_id, :string
  end
end
