class CreateFaqs < ActiveRecord::Migration
  def change
    create_table :faqs do |t|
      t.string :question
      t.text :answer
      t.references :institute_profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
