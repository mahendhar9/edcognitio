class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :city
      t.text :address
      t.string :phone_number
      t.string :state
      t.integer :pincode
      t.references :institute_profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
