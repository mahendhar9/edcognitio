class CreatePlacements < ActiveRecord::Migration
  def change
    create_table :placements do |t|
      t.references :course, index: true, foreign_key: true
      t.integer :year
      t.string :student_name
      t.string :company_name
      t.references :institute_profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
