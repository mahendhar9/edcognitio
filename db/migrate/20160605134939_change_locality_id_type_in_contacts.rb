class ChangeLocalityIdTypeInContacts < ActiveRecord::Migration
  def change
   remove_column :contacts, :locality_id
   add_column :contacts, :locality_id, :integer
 end
end
