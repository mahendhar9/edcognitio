class AddBrochureToInstituteProfiles < ActiveRecord::Migration
  def change
    add_column :institute_profiles, :brochure, :string
  end
end
